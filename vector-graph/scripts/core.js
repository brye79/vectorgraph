function initGraph(container, domain, range){
	var tickInterval = 0.5;
	container.setAttribute("class", "vector-graph");
	
	var svg = document.createElementNS("http://www.w3.org/2000/svg","svg");
	container.appendChild(svg);
	svg.setAttribute("height", 500);
	svg.setAttribute("width", 500);
	svg.setAttribute("viewBox", (-1 * domain) + " " + (-1 * range) + " " + (2 * domain) + " " + (2 * range));
	
	var xAxis = document.createElementNS("http://www.w3.org/2000/svg","path");
	xAxis.setAttribute("d", "M" + (-1 * domain) + " 0 L" + domain + " 0");
	xAxis.setAttribute("stroke", "#606060");
	xAxis.setAttribute("stroke-width", "0.03");
	xAxis.setAttribute("fill", "none");
	svg.appendChild(xAxis);
	for(var x = -1 * domain; x <= domain; x++){
		for(var j = x; j < x+1; j += tickInterval){
			var tick = document.createElementNS("http://www.w3.org/2000/svg","path");
			if(j == x){
				tick.setAttribute("d", "M" + j + " 0.1 l0 -0.2");
				tick.setAttribute("stroke-width", "0.03");
			}
			else{
				tick.setAttribute("d", "M" + j + " 0.05 l0 -0.1");
				tick.setAttribute("stroke-width", "0.03");
			}
				
			tick.setAttribute("stroke", "#606060");
			tick.setAttribute("fill", "none");
			svg.appendChild(tick);
		}
	}
	
	var yAxis = document.createElementNS("http://www.w3.org/2000/svg","path");
	yAxis.setAttribute("d", "M0 " + (-1 * range) + " L0 " + range);
	yAxis.setAttribute("stroke", "#606060");
	yAxis.setAttribute("stroke-width", "0.03");
	yAxis.setAttribute("fill", "none");
	svg.appendChild(yAxis);
	for(var y = -1 * range; y <= range; y++){
		for(var j = y; j < y+1; j += tickInterval){
			var tick = document.createElementNS("http://www.w3.org/2000/svg","path");
			if(j == y){
				tick.setAttribute("d", "M0.1 " + j + " l-0.2 0");
				tick.setAttribute("stroke-width", "0.03");
			}
			else{
				tick.setAttribute("d", "M0.05 " + j + " l-0.1 0");
				tick.setAttribute("stroke-width", "0.03");
			}
				
			tick.setAttribute("stroke", "#606060");
			tick.setAttribute("fill", "none");
			svg.appendChild(tick);
		}
	}
	
	return {
		graph: svg,
		domain: domain, 
		range: range,
		plot: function(func, color){ 
			var pathPrefix = "M";
			var d = "";
			var x = -1.0 * this.domain;
			while(x <= this.domain){
				var y = Math.round(func(x) * 100) / 100;
				if(Math.abs(y) <= this.range){
					d += pathPrefix + String(Math.round(x * 100) / 100) + " " + String(-1 * y);
					pathPrefix = " L";
				}
				x += 0.01;
			}
			console.log(d);
			var path = document.createElementNS("http://www.w3.org/2000/svg","path");
			path.setAttribute("d", d);
			path.setAttribute("stroke", color);
			path.setAttribute("stroke-width", "0.05");
			path.setAttribute("fill", "none");
			this.graph.appendChild(path);
		}
	};
}
 